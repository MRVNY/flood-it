#ifndef __GRAPHE_ZONE__
#define __GRAPHE_ZONE__

#include <stdio.h>
#include <stdlib.h>
#include "Liste_case.h"
#include "Entete_Fonctions.h"

typedef struct sommet Sommet;

typedef struct cellule_som {
	Sommet * sommet;
	struct cellule_som * suiv;
} Cellule_som;

struct sommet {
	int num; /* Numero du sommet (sert uniquement a l’affichage) */
	int cl;
	ListeCase cases;
	int nbcase_som; /* Nombre de cases de cette liste */
	int marque; //il permet d’indiquer le statut d’un sommet
	int distance; //distance par rapport a la racine
	Sommet *pere; //vient avant dans le parcours en largeur

	Cellule_som * sommet_adj;
};

typedef struct graphe_zone{
	int nbsom;
	Cellule_som *som;
	Sommet ***mat; /* Matrice de pointeurs sur les sommets indiquant a quel sommet appartient une case (i,j) de la grille */
} Graphe_zone;

typedef struct bordure_graphe{
	int nbcl;
	Cellule_som *Z; //il est egalement utile de stocker la liste des sommets composant la Zsg.
	Cellule_som **B; //un tableau de nbcl listes chainees de pointeurs sur sommets-zones
	int *taille; //la taille de chaque liste correspondant aux couleurs repr ́esent ́ees dans la bordure.
} Bordure_graphe;

Cellule_som* ajoute_liste_sommet(Cellule_som *liste, Sommet *s) ;

void detruit_liste_sommet(Cellule_som *liste);

void ajout_voisin(Sommet *s1, Sommet *s2);

int adjacent(Sommet *s1, Sommet *s2);

Graphe_zone* creer_graphe_zone(int **M, int dim);

void affiche_graphe(Graphe_zone *graphe);

void trouve_zone(int **M, Graphe_zone *graphe, int i, int j, int nbcase, ListeCase *L);

void ajoute_zsg_graphe(Bordure_graphe *BG, Cellule_som *C);

void ajoute_bordure_graphe(Bordure_graphe *BG, Cellule_som *C);

int bordure_est_vide(Bordure_graphe *BG);

Bordure_graphe *init_bordure(int nbcl, Graphe_zone *G);

void maj_bordure(Bordure_graphe *BG, int cl);

void parcours_largeur(Graphe_zone *G);

void detruit_graphe(Graphe_zone *G, int dim);

void detruit_bordure(Bordure_graphe *BG, int nbcl);

#endif