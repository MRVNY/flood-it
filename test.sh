make clean
make Flood-It

exo=$1
scale=$2
echo "exo=$exo"

#dim temps
echo dim
nbcl=5 #par defaut
for ((i=1;i<11;i++));do
    somt=0
    some=0
    dim=$(($i * $scale))
    echo "dim=$dim:"
    for ((graine=1;graine<6;graine++));do
        res=$(./Flood-It $dim $nbcl 5 $graine $exo 0)
        t=$(echo $res | cut -d ' ' -f 3 | bc)
        e=$(echo $res | grep 'essais' | cut -d ' ' -f 1 | bc)
        somt="$somt + $t"
        some="$some + $e"
    done
    temps=$(echo "scale=4;( $somt ) / 5" | bc)
    essais=$(echo "scale=1;( $some ) / 5" | bc)
    echo temps=$temps essais=$essais
done

#nbcl 
echo nbcl
dim=$((5 * $scale)) #par defaut
for ((nbcl=2;nbcl<12;nbcl++));do
    somt=0
    some=0
    echo "nbcl=$nbcl"
    for ((graine=1;graine<6;graine++));do
        res=$(./Flood-It $dim $nbcl 5 $graine $exo 0)
        t=$(echo $res | cut -d ' ' -f 3 | bc)
        e=$(echo $res | grep 'essais' | cut -d ' ' -f 1 | bc)
        somt="$somt + $t"
        some="$some + $e"
    done
    temps=$(echo "scale=4;( $somt ) / 5" | bc)
    essais=$(echo "scale=1;( $some ) / 5" | bc)
    echo temps=$temps essais=$essais
done

make clean
