#include <stdio.h>
#include "Entete_Fonctions.h"
#include "API_Grille.h"

//1.1
void trouve_zone_rec(int **M, int nbcase, int i, int j, int *taille, ListeCase *L)
{

	int cl = M[0][0]; //on vérifie que la case n'est pas dans la liste puis on l'ajoute
	if(!est_dans_liste(L, i, j)) {
		ajoute_en_tete(L, i, j);
		*taille = *taille + 1;
	}
	//on rappel trouve_zone_rec si une case de la même couleur est à côté
	//on verifie que la case n'est pas déjà dans la liste et qu'on ne dépasse pas les dimensions
	if((i+1 < nbcase) && (!est_dans_liste(L, i+1, j))) {
		if(M[i+1][j] == cl) {
			trouve_zone_rec(M, nbcase, i+1, j, taille, L);
		}
	}

	if((i-1 >= 0) && (!est_dans_liste(L, i-1, j))) {
		if(M[i-1][j] == cl) {
			trouve_zone_rec(M, nbcase, i-1, j, taille, L);
		}
	}

	if((j+1 < nbcase) && (!est_dans_liste(L, i, j+1))) {
		if(M[i][j+1] == cl) {
			trouve_zone_rec(M, nbcase, i, j+1, taille, L);
		}
	}

	if((j-1 >= 0) && (!est_dans_liste(L, i, j-1))) {
		if(M[i][j-1] == cl) {
			trouve_zone_rec(M, nbcase, i, j-1, taille, L);
		}
	}
}

//1.2
int sequence_aleatoire_rec(int **M, Grille *G, int dim, int nbcl, int aff)
{

  int cpt=0, taille = 1;
  int cl;
  Elnt_liste *L = NULL, *pL;
  ajoute_en_tete(&L, 0, 0);

  while(taille < (dim*dim)) {
  	cl = (int)(rand() % nbcl);
  	M[0][0] = cl;
  	pL = L;
  	while(pL) {   //on utilise trouve_zone_rec pour chaque élément de la liste
  		trouve_zone_rec(M, dim, pL->i, pL->j, &taille, &L);
  		pL = pL->suiv;
  	}
  	pL = L;
  	while(pL) {  //on change la couleur de toute les cases de la liste 
  		M[pL->i][pL->j] = cl;
  		if(aff==1) Grille_attribue_couleur_case(G,pL->i,pL->j,cl);
  		pL=pL->suiv;
  	}
  	if(aff==1) Grille_redessine_Grille(G);
  	cpt++;
  }

  detruit_liste(&L);

  return cpt;
}

//2.1
void trouve_zone_imp(int **M, int nbcase, int i, int j, int *taille, ListeCase *L){
	ListeCase todo;
	ajoute_en_tete(&todo,-1,-1);
	ajoute_en_tete(&todo,i,j);
	int cl = M[0][0],x,y;

	while(todo->i!=-1){
		enleve_en_tete(&todo,&i,&j);
		for(x=-1;x<2;x++){
			for(y=-1;y<2;y++){
				if(((x==0 && y!=0)||(x!=0 && y==0))
				&& i+x>=0 && i+x<nbcase && j+y>=0 && j+y<nbcase
				&& !est_dans_liste(L,i+x,j+y) && M[i+x][j+y]==cl){
					ajoute_en_tete(&todo,i+x,j+y);
					ajoute_en_tete(L,i+x,j+y);
					(*taille)++;
				}
			}
		}
	}
}

//2.2
int sequence_aleatoire_imp(int **M, Grille *G, int dim, int nbcl, int aff){
	ListeCase L = NULL, pL;
	int taille = 1, cpt = 0, cl;
	ajoute_en_tete(&L, 0, 0);
	trouve_zone_imp(M, dim, 0, 0, &taille, &L);

	while(taille < dim*dim){
		cl = (int)(rand()%nbcl);
		M[0][0] = cl;
		pL = L;
		while(pL){
			trouve_zone_imp(M, dim, pL->i, pL->j, &taille, &L);
			pL = pL->suiv;
		}
		pL = L;
		while(pL){    //on redessine toute la zone
			M[pL->i][pL->j] = cl; 
			if(aff==1) Grille_attribue_couleur_case(G,pL->i,pL->j,cl);
			pL = pL->suiv;
		}
		cpt++;
		if(aff==1) Grille_redessine_Grille(G);
	}
	detruit_liste(&L);
	return cpt;
}

//3.3
int strequence_aleatoire_rapide(int **M, Grille *G, int dim, int nbcl, int aff){
	Elnt_liste *tmp;
	int cl = M[0][0], i = 0, j = 0, taille = 0, cpt = 0;
	S_Zsg *Z = init_Zsg(dim, nbcl);
	taille += agrandit_Zsg(M,Z,cl,0,0);

	while(taille<dim*dim){
		cl = (int)(rand() % nbcl);
		tmp = Z->Lzsg;
		while(tmp){ //colorier toutes les cases de la zone Zsg a la couleur cl dans M
			M[tmp->i][tmp->j] = Z->App[tmp->i][tmp->j] = cl;
			if(aff==1) Grille_attribue_couleur_case(G,tmp->i,tmp->j,cl);
			tmp = tmp->suiv;
		}
		tmp = Z->B[cl];
		while(tmp){
			taille += agrandit_Zsg(M,Z,cl,tmp->i,tmp->j);
			if(aff==1) Grille_attribue_couleur_case(G,tmp->i,tmp->j,cl);
			tmp = tmp->suiv;
		}
		detruit_liste(&Z->B[cl]);
		if(aff==1) Grille_redessine_Grille(G);
		cpt++;
	}
	detruit_Zsg(Z);
	return cpt;
}

//5.2
int max_bordure(int **M, Grille *G, int dim, int nbcl, int aff){
	//Initialisation
	Graphe_zone *GZ = creer_graphe_zone(M,dim);
	Bordure_graphe *BG = init_bordure(nbcl,GZ);
	int cl,i,max=-1,cpt=0;

	Cellule_som *tmpC;
	Elnt_liste *tmpL;
	//maj de bordure & grille
	while(max!=0){
		//Trouver le sommet le plus grand dans bordure
		max = 0;
		for(i=0;i<nbcl;i++){
			if(BG->taille[i]>max){
				max = BG->taille[i];
				cl = i;
			}
		}
		if(max==0) break;
		maj_bordure(BG,cl);

		//maj de M et G
		tmpC = BG->Z;
		while(tmpC){
			tmpL = tmpC->sommet->cases;
			while(tmpL){
				M[tmpL->i][tmpL->j] = cl;
				if(aff==1) Grille_attribue_couleur_case(G,tmpL->i,tmpL->j,cl);
				tmpL = tmpL->suiv;
			}
			tmpC = tmpC->suiv;
		}
		if(aff==1) Grille_redessine_Grille(G);
		cpt++;
	}

	detruit_bordure(BG, nbcl);
	detruit_graphe(GZ, dim);

	return cpt;
}

//6.2
int max_bordure_rapide(int **M, Grille *G, int dim, int nbcl, int aff){
	//Initialisation
	Graphe_zone *GZ = creer_graphe_zone(M,dim);
	parcours_largeur(GZ);
	Bordure_graphe *BG = init_bordure(nbcl,GZ);
	int cl,i,max=-1,cpt=0;

	Cellule_som *tmpC;
	Elnt_liste *tmpL;

	Sommet *coin_inf_droit = GZ->mat[dim-1][dim-1]; //on cherche le sommet du coin inférieur droit
	cpt = cpt + coin_inf_droit->distance; //on ajoute la distance du coin inferieur droit au compteur

	Sommet **tab_sommet = (Sommet**)malloc(sizeof(Sommet*)*coin_inf_droit->distance);

	for(i=coin_inf_droit->distance-1;i>=0;i--) { 
		tab_sommet[i] = coin_inf_droit;			//on stocke la chaine de sommets
		coin_inf_droit = coin_inf_droit->pere;
	}

	for(i=0;i<cpt;i++) { // cpt = coin_inf_droit->distance
		cl = tab_sommet[i]->cl;
		maj_bordure(BG, tab_sommet[i]->cl);

		tmpC = BG->Z;
		while(tmpC){
			tmpL = tmpC->sommet->cases;
			while(tmpL){
				M[tmpL->i][tmpL->j] = cl;
				if(aff==1) Grille_attribue_couleur_case(G,tmpL->i,tmpL->j,cl);
				tmpL = tmpL->suiv;
			}
			tmpC = tmpC->suiv;
		}
		if(aff==1) Grille_redessine_Grille(G);
	}

	free(tab_sommet);

	//maj de bordure & grille
	while(max!=0){
		//comparer taille de chaque cl dans bordure
		max = 0;
		for(i=0;i<nbcl;i++){
			if(BG->taille[i]>max){
				max = BG->taille[i];
				cl = i;
			}
		}
		if(max==0) break;
		maj_bordure(BG,cl);

		tmpC = BG->Z;
		while(tmpC){
			tmpL = tmpC->sommet->cases;
			while(tmpL){
				M[tmpL->i][tmpL->j] = cl;
				if(aff==1) Grille_attribue_couleur_case(G,tmpL->i,tmpL->j,cl);
				tmpL = tmpL->suiv;
			}
			tmpC = tmpC->suiv;
		}
		if(aff==1) Grille_redessine_Grille(G);
		cpt++;
	}

	detruit_bordure(BG, nbcl);
	detruit_graphe(GZ, dim);

	return cpt;
}