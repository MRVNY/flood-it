#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Liste_case.h"
#include "S_Zsg.h"

S_Zsg *init_Zsg(int dim, int nbcl){
    S_Zsg *new = (S_Zsg*)malloc(sizeof(S_Zsg));
    new->dim = dim;
    new->nbcl = nbcl;
    new->Lzsg = NULL;
    
    new->App = (int **)malloc(sizeof(int*)*dim);
    int i, j;
    for(i=0; i<dim; i++) {
    	new->App[i] = (int*)malloc(sizeof(int)*dim);
    	for(j=0;j<dim;j++) {
    		new->App[i][j] = -2;
    	}
    }

    new->B = (Elnt_liste**)malloc(sizeof(Elnt_liste*)*nbcl);
    for(i=0;i<nbcl;i++) {
    	new->B[i] = NULL;
    }
    
    return new;
}

void detruit_Zsg(S_Zsg *Z)
{
	detruit_liste(&Z->Lzsg);
	int i;
	for(i=0;i<Z->nbcl;i++) {
		detruit_liste(&Z->B[i]);
	}
	for(i=0;i<Z->dim;i++) {
		free(Z->App[i]);
	}
	free(Z->B);
	free(Z->App);
	free(Z);
}

void ajoute_Zsg(S_Zsg *Z, int i, int j, int cl){
    ajoute_en_tete(&Z->Lzsg,i,j);
    Z->App[i][j] = cl;
}

void ajoute_Bordure(S_Zsg *Z, int i, int j, int cl){
    ajoute_en_tete(&Z->B[cl],i,j);
    Z->App[i][j] = -1;
}

int appartient_Zsg(S_Zsg *Z, int i, int j){
    return (Z->App[i][j]>=0);
}

int appartient_Bordure(S_Zsg *Z, int i, int j){
    return (Z->App[i][j]==-1);
}

int agrandit_Zsg(int **M, S_Zsg *Z, int cl, int k, int l){
    ListeCase todo; //todo: liste de cases a traiter
    int i,j,cpt = 0,x,y;
    ajoute_en_tete(&todo,-1,-1);
	ajoute_en_tete(&todo,k,l);
    if(!appartient_Zsg(Z,k,l)){
        ajoute_Zsg(Z, k, l, cl);
        cpt++;
    }

    while(todo->i!=-1){
        enleve_en_tete(&todo,&i,&j);
        for(x=-1;x<2;x++){
			for(y=-1;y<2;y++){
				if(((x==0 && y!=0)||(x!=0 && y==0))
				&& i+x>=0 && i+x<Z->dim && j+y>=0 && j+y<Z->dim){
                    if(!appartient_Zsg(Z,i+x,j+y) && M[i+x][j+y]==cl){
                        ajoute_en_tete(&todo,i+x,j+y);
                        ajoute_Zsg(Z,i+x,j+y,cl);
                        cpt++;
                    }
                    else if(M[i+x][j+y]!=cl){
                        ajoute_Bordure(Z,i+x,j+y,M[i+x][j+y]);
                    }
                }
			}
		}
    }
    return cpt;
}