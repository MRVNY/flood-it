#include "Graphe_zone.h"

Cellule_som* ajoute_liste_sommet(Cellule_som *liste, Sommet *s) 
{
	Cellule_som *new = (Cellule_som*)malloc(sizeof(Cellule_som));
	new->sommet = s;
	new->suiv = liste;

	return new;
}

void detruit_liste_sommet(Cellule_som *liste)
{
	Cellule_som *tmp;
	while(liste != NULL){
		tmp = liste;
		liste = liste->suiv;
		free(tmp);
	}
}

void ajout_voisin(Sommet *s1, Sommet *s2)
{
	s2->sommet_adj = ajoute_liste_sommet(s2->sommet_adj, s1);
	s1->sommet_adj = ajoute_liste_sommet(s1->sommet_adj, s2);
}

int adjacent(Sommet *s1, Sommet *s2)
{
	Cellule_som *tmp = s1->sommet_adj;

	while(tmp) {
		if(tmp->sommet == s2) {
			return 1;
		}
		tmp = tmp->suiv;
	}

	return 0;
}

void trouve_zone(int **M, Graphe_zone *graphe, int i, int j, int nbcase, ListeCase *L)
{
	int cl = M[i][j];
	if(M[i][j]!=-1) {
		ajoute_en_tete(L, i, j);
		M[i][j] = -1;
	}
	
	if((i+1 < nbcase) && (M[i+1][j]!=-1) && (graphe->mat[i+1][j] == NULL)) {
		if(M[i+1][j] == cl) {
			trouve_zone(M, graphe, i+1, j, nbcase, L);
		}
	}

	if((i-1 >= 0) && (M[i-1][j]!=-1) && (graphe->mat[i-1][j] == NULL)) {
		if(M[i-1][j] == cl) {
			trouve_zone(M, graphe, i-1, j, nbcase, L);
		}
	}

	if((j+1 < nbcase) && (M[i][j+1]!=-1) && (graphe->mat[i][j+1] == NULL)) {
		if(M[i][j+1] == cl) {
			trouve_zone(M, graphe, i, j+1, nbcase, L);
		}
	}

	if((j-1 >= 0) && (M[i][j-1]!=-1) && (graphe->mat[i][j-1] == NULL)) {
		if(M[i][j-1] == cl) {
			trouve_zone(M, graphe, i, j-1, nbcase, L);
		}
	}
}

Graphe_zone* creer_graphe_zone(int **M, int dim)
{
 	int cpt = 0;

 	ListeCase l;

 	Graphe_zone *graphe = (Graphe_zone*) malloc(sizeof(Graphe_zone));
 	graphe->nbsom = 0;
 	graphe->som = NULL;

 	int i, j;

 	graphe->mat = (Sommet***)malloc(sizeof(Sommet**)*dim);
 	for(i=0; i<dim; i++) {
 		graphe->mat[i] = (Sommet**)malloc(sizeof(Sommet*)*dim);
 		for(j=0; j<dim; j++) {
 			graphe->mat[i][j] = NULL;
 		}
 	}

 	for(i=0; i<dim; i++) {
 		for(j=0; j<dim; j++) {
 			if(graphe->mat[i][j] == NULL) {
 				Sommet *s = (Sommet*) malloc(sizeof(Sommet)); //on crée un sommet
 				s->num = cpt;
 				cpt++;
 				s->cl = M[i][j];
 				s->cases = NULL;
 				s->nbcase_som = 0;
 				s->sommet_adj = NULL;
				s->marque = 2;
				s->distance = 999999999;
				s->pere = NULL;
 				trouve_zone(M, graphe, i, j, dim, &(s->cases)); //on ajoute au sommet toutes les cases adjacentes de même couleur
 				graphe->som = ajoute_liste_sommet(graphe->som, s); //on ajoute le sommet au graphe

 				//on actualise la matrice du graphe
 				l = s->cases;
 				while(l) {
 					graphe->mat[l->i][l->j] = s;
 					s->nbcase_som = s->nbcase_som + 1;
 					l = l->suiv;
 				}
 				s = NULL;
 			}
 		}
 	}

 	//on s'occupe des voisins, on parcours chaque cases de la matrice et on ajoute les sommets adjacents 
 	//dans la liste des voisins si ils n'y sont pas déjà
 	for(i=0; i<dim; i++) {
 		for(j=0; j<dim; j++) {

 			if(i>0) {
 				if((graphe->mat[i][j] != graphe->mat[i-1][j]) && (!adjacent(graphe->mat[i][j], graphe->mat[i-1][j]))) {
 					ajout_voisin(graphe->mat[i][j],graphe->mat[i-1][j]);
 				}
 			}

 			if(i<dim-1) {
 				if((graphe->mat[i][j] != graphe->mat[i+1][j]) && (!adjacent(graphe->mat[i][j], graphe->mat[i+1][j]))) {
 					ajout_voisin(graphe->mat[i][j],graphe->mat[i+1][j]);
 				}
 			}

 			if(j>0) {
 				if((graphe->mat[i][j] != graphe->mat[i][j-1]) && (!adjacent(graphe->mat[i][j], graphe->mat[i][j-1]))) {
 					ajout_voisin(graphe->mat[i][j],graphe->mat[i][j-1]);
 				}
 			}

 			if(j<dim-1) {
 				if((graphe->mat[i][j] != graphe->mat[i][j+1]) && (!adjacent(graphe->mat[i][j], graphe->mat[i][j+1]))) {
 					ajout_voisin(graphe->mat[i][j],graphe->mat[i][j+1]);
 				}
 			}
 		}
 	}

 	return graphe;
 }

 void detruit_graphe(Graphe_zone *G, int dim) 
 {

 	Cellule_som *tmp = G->som, *tmp2 = NULL, *tmp3 = NULL;
 	Sommet *tmpsom;

 	while(tmp) {
 		tmpsom = tmp->sommet;
 		detruit_liste(&(tmpsom->cases)); //on free les cases du sommet

 		tmp2 = tmpsom->sommet_adj; //on free les sommets adjacents
 		while(tmp2) {
 			tmp3 = tmp2->suiv;
 			free(tmp2);
 			tmp2 = tmp3;
 		}
 		free(tmpsom);   //on free le sommet
 		tmp2 = tmp->suiv;
 		free(tmp);
 		tmp = tmp2;
 	}

 	int i;
 	for(i=0;i<dim;i++) { //on free la matrice
 		free(G->mat[i]);
 	}
 	free(G->mat);

 	free(G);
 }

void affiche_graphe(Graphe_zone *graphe)
{
	Cellule_som *tmp, *tmp2 ;

	//matrice sommet-sommet
	printf("matrice sommet-sommet:\n");
	printf("\t");
	tmp = graphe->som;
	while(tmp){
		printf("(%d)\t",tmp->sommet->num);
		tmp = tmp->suiv;
	}
	printf("\n");
	tmp = graphe->som;
	while(tmp){
		tmp2 = graphe->som;
		printf("(%d)\t",tmp->sommet->num);
		while(tmp2){
			printf(" %d\t",adjacent(tmp->sommet,tmp2->sommet));
			tmp2 = tmp2->suiv;
		}
		printf("\n");
		tmp = tmp->suiv;
	}

	//liste d'adjacence
	printf("liste d'adjacence:\n");
	tmp = graphe->som;
	while(tmp) {
		tmp2 = tmp->sommet->sommet_adj;
		printf("Le sommet %d est voisins de :\n", tmp->sommet->num);
		while(tmp2) {
			printf("%d\t", tmp2->sommet->num);
			tmp2 = tmp2->suiv;
		}
		tmp = tmp->suiv;
		printf("\n");
	}
}

//5.1
void ajoute_zsg_graphe(Bordure_graphe *BG, Cellule_som *C){
	BG->Z = ajoute_liste_sommet(BG->Z,C->sommet);
	C->sommet->marque = 0;
}

void ajoute_bordure_graphe(Bordure_graphe *BG, Cellule_som *C){
	BG->B[C->sommet->cl] = ajoute_liste_sommet(BG->B[C->sommet->cl],C->sommet);
	C->sommet->marque = 1;
	BG->taille[C->sommet->cl] += C->sommet->nbcase_som;
}

Bordure_graphe *init_bordure(int nbcl, Graphe_zone *G){
	Bordure_graphe *new = (Bordure_graphe*)malloc(sizeof(Bordure_graphe));
	new->nbcl = nbcl;
	
	//initialise Zsg
	new->Z = NULL;
	Cellule_som *tmp = G->som;
	while(tmp){
		if(tmp->sommet->num==0){
			ajoute_zsg_graphe(new,tmp);
			break;
		}
		else tmp = tmp->suiv;
	}

	//initialise bordure
	new->B = (Cellule_som**)malloc(nbcl*sizeof(Cellule_som*));
	new->taille = (int*)malloc(nbcl*sizeof(int));
	int i;
	for(i=0;i<nbcl;i++){
		new->B[i] = NULL;
		new->taille[i] = 0;
	}

	tmp = new->Z->sommet->sommet_adj;
	while(tmp){
		if(tmp->sommet->marque==2) ajoute_bordure_graphe(new,tmp);
		tmp = tmp->suiv;
	}

	return new;
}

void detruit_bordure(Bordure_graphe *BG, int nbcl)
{

	detruit_liste_sommet(BG->Z);
	free(BG->taille);

	int i;
	for(i=0;i<nbcl;i++) {
		detruit_liste_sommet(BG->B[i]);
	}
	
	free(BG->B);
	free(BG);
}

void maj_bordure(Bordure_graphe *BG, int cl){
	Cellule_som *tmp = BG->B[cl], *tmpadj;
	while(tmp && tmp->sommet){
		//maj de bordure
		tmpadj = tmp->sommet->sommet_adj;
		while(tmpadj){ //parcourir sommet_adj de chaque sommet dans la bordure
			if(tmpadj->sommet->marque==2) ajoute_bordure_graphe(BG,tmpadj);
			tmpadj = tmpadj->suiv;
		}
		
		//maj de Zsg
		ajoute_zsg_graphe(BG,tmp);
		tmp = tmp->suiv;
	}
	//liberer liste de cl dans bordure
	detruit_liste_sommet(BG->B[cl]);
	BG->B[cl] = NULL;
	BG->taille[cl] = 0;
}

void parcours_largeur(Graphe_zone *G) {

	Sommet *racine = G->mat[0][0]; //la racine est la case 0,0
	Cellule_som *tmp, *tmp1;
	racine->distance = 0;
	Cellule_som *todo = NULL, *todo1 = NULL;
	todo = ajoute_liste_sommet(todo, racine);
	int cpt = 1;

	while(todo) { //on parcours la liste de sommets
		
		tmp = todo;

		while(tmp) { //on parcours la liste des sommets adjacents de chaque sommet
			tmp1 = tmp->sommet->sommet_adj;

			while(tmp1) {
				if(tmp1->sommet->distance == 999999999) {  //on initialise le pere et la distance des sommets adjacents
					tmp1->sommet->distance = cpt;
					tmp1->sommet->pere = tmp->sommet;
					todo1 = ajoute_liste_sommet(todo1, tmp1->sommet);
				}
				tmp1 = tmp1->suiv;
			}
			tmp1 = tmp;
			tmp = tmp->suiv;
			free(tmp1); //on defile
		}
		cpt++;
		todo = todo1;
		todo1 = NULL;
	}
}