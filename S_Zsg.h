#ifndef __S_ZSG__
#define __S_ZSG__

#include "Liste_case.h"

typedef struct s_zsg{
    int dim; /* dimension de la grille */
    int nbcl; /* nombre de couleurs */
    ListeCase Lzsg; /* Liste des cases de la zone Zsg */
    ListeCase *B; /* Tableau de listes de cases de la bordure*/
    int **App; /* Tableau a double entree des appartenances */
} S_Zsg;


//initialise la structure
S_Zsg *init_Zsg(int dim, int nbcl);

//detruit la Zsg Z
void detruit_Zsg(S_Zsg *Z);

//ajoute une case dans la liste Lzsg
void ajoute_Zsg(S_Zsg *Z, int i, int j, int cl);

//ajoute une case dans la bordure d'une couleur cl donnee
void ajoute_Bordure(S_Zsg *Z, int i, int j, int cl);

//qui renvoie vrai si une case est dans LZsg
int appartient_Zsg(S_Zsg *Z, int i, int j);

//renvoie vrai si une case est dans la bordure de couleur cl donnee
int appartient_Bordure(S_Zsg *Z, int i, int j);


int agrandit_Zsg(int **M, S_Zsg *Z, int cl, int k, int l);


#endif